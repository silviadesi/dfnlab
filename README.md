# DFNLab

Welcome to the Gitlab page dedicated to DFN.lab.

## Description

DFNLab is a software suite for Discrete Fracture Network analyses. It includes various modules for generation, analyses, flow, transport and mechanics.
See https://www.fractorylab.com for more details.

## The Team

DFNLab is developed by the *Fractory*, a joint laboratory between Itasca Consultants, the CNRS and the University of Rennes.

## Where is the code

The code is not freely available, please contact r.legoc@itasca.fr or philippe.davy@univ-rennes1.fr to ask for an access. We provide  free access only to collaborators in academic research.

You will find a this page some tutorials on the use of DFN.lab that you can run once you get the code. You can also submit tickets if you have any question/bugs.

